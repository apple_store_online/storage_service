package service

import (
	"context"
	"fmt"
	pb "storage_service/genproto/storage_service_protos"
	"storage_service/grpc/client"
	"storage_service/storage"
	"google.golang.org/protobuf/types/known/emptypb"
)

type productService struct {
	storage storage.IStorage
	services client.IServiceManager
	pb.UnimplementedProductServiceServer
}

func NewProductService(storage storage.IStorage, services client.IServiceManager) *productService {
	return &productService{
		storage: storage,
		services: services,
	}
}

func (p *productService) Create(ctx context.Context, req *pb.CreateProductRequest) (*pb.Product, error) {
	product, err := p.storage.Product().Create(ctx, req)
	if err != nil{
		fmt.Println("Error in service layer, while creating a product!", err.Error())
		return &pb.Product{}, nil
	}

	return product, nil
}

func (p *productService) Get(ctx context.Context, req *pb.ProductPrimaryKey) (*pb.Product, error) {
	product, err := p.storage.Product().Get(ctx, req)
	if err != nil{
		fmt.Println("Error in service layer, while getting product!", err.Error())
		return &pb.Product{}, err
	}
	return product, nil
}

func (p *productService) GetList(ctx context.Context, req *pb.GetProductListRequest) (*pb.ProductsResponse, error) {
	products, err := p.storage.Product().GetList(ctx, req)
	if err != nil{
		fmt.Println("Error in service layer, while getting all products!", err.Error())
		return &pb.ProductsResponse{}, err
	}

	return products, nil
}  

func (p *productService) Update(ctx context.Context, req *pb.Product) (*pb.Product, error) {
	product, err := p.storage.Product().Update(ctx, req)
	if err != nil{
		fmt.Println("Error in service layer, while updating product!", err.Error())
		return &pb.Product{}, err
	}

	return product, nil
}

func (p *productService) Delete(ctx context.Context, req *pb.ProductPrimaryKey) (*emptypb.Empty, error) {
	err := p.storage.Product().Delete(ctx, req)
	if err != nil{
		fmt.Println("Error in service layer, while deleting product!",err.Error())
		return &emptypb.Empty{}, err
	}

	return &emptypb.Empty{},nil
}