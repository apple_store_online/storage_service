package service

import (
	"context"
	"fmt"
	pb "storage_service/genproto/storage_service_protos"
	"storage_service/grpc/client"
	"storage_service/storage"	
	"google.golang.org/protobuf/types/known/emptypb"
)

type storageProductService struct {
	storage storage.IStorage
	services client.IServiceManager
	pb.UnimplementedStorageProductServiceServer
}

func NewStorageProductService(storage storage.IStorage, services client.IServiceManager) *storageProductService {
	return &storageProductService{
		storage: storage,
		services: services,
	}
}

func (s *storageProductService) Create(ctx context.Context, req *pb.CreateStorageProductRequest) (*pb.StorageProduct, error) {
	storageProduct, err := s.storage.StorageProduct().Create(ctx, req)
	if err != nil{
		fmt.Println("Error in service layer, while creating storage product!", err.Error())
		return &pb.StorageProduct{}, err
	}

	return storageProduct, nil
}

func (s *storageProductService) Get(ctx context.Context, req *pb.StorageProductPrimaryKey) (*pb.StorageProduct, error) {
	storageProduct, err := s.storage.StorageProduct().Get(ctx, req)
	if err != nil{
		fmt.Println("Error in service layer, while getting storage product!", err.Error())
		return &pb.StorageProduct{}, err
	}

	return storageProduct, nil
}

func (s *storageProductService) GetList(ctx context.Context, req *pb.GetStorageProductListRequest) (*pb.StorageProductsResponse, error) {
	storageProducts, err := s.storage.StorageProduct().GetList(ctx, req)
	if err != nil{
		fmt.Println("Error in service layer, while getting list of storage products!", err.Error())
		return &pb.StorageProductsResponse{}, err
	}

	return storageProducts, nil
}

func (s *storageProductService) Update(ctx context.Context, req *pb.StorageProduct) (*pb.StorageProduct, error) {
	storageProduct, err := s.storage.StorageProduct().Update(ctx, req)
	if err != nil{
		fmt.Println("Error in service layer, while updating storage product!", err.Error())
		return &pb.StorageProduct{}, err
	}

	return storageProduct, nil
}

func (s *storageProductService) Delete(ctx context.Context, req *pb.StorageProductPrimaryKey) (*emptypb.Empty, error) {
	err := s.storage.StorageProduct().Delete(ctx, req)
	if err != nil{
		fmt.Println("Error in service layer, while deleting storage product!", err.Error())
		return &emptypb.Empty{}, err
	}

	return &emptypb.Empty{}, nil
}
