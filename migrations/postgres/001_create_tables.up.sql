CREATE TABLE IF NOT EXISTS categories (
    id uuid primary key,
    name varchar(60) unique NOT NULL,
    created_at timestamp DEFAULT NOW(),
    updated_at timestamp DEFAULT NOW(),
    deleted_at integer DEFAULT 0
);

CREATE TABLE IF NOT EXISTS products (
    id uuid primary key,
    name varchar(100) unique NOT NULL,
    category_id uuid references categories(id),
    created_at timestamp DEFAULT NOW(),
    updated_at timestamp DEFAULT NOW(),
    deleted_at integer DEFAULT 0
);

CREATE TABLE IF NOT EXISTS storage_products (
    id uuid primary key,
    product_id uuid references products(id),
    quantity int,
    color varchar(20) NOT NULL,
    original_price float NOT NULL,
    selling_price float,
    created_at timestamp DEFAULT NOW(),
    updated_at timestamp DEFAULT NOW(),
    deleted_at integer DEFAULT 0
);

 