package main

import (
	"context"
	"fmt"
	"net"
	"storage_service/config"
	"storage_service/grpc"
	"storage_service/grpc/client"
	"storage_service/storage/postgres"
)

func main(){
	cfg := config.Load()

	postgresStore, err := postgres.New(context.Background(),cfg)
	if err != nil{
		fmt.Println("Error while connecting to database!", err.Error())
		return
	}

	defer postgresStore.Close()

	services, err := client.NewGrpcClients(cfg)
	if err != nil{
		fmt.Println("Error while dialing grpc clients!",err.Error())
		return
	}
	

	grpcServer := grpc.SetUpServer(postgresStore, services)

	lis, err := net.Listen("tcp",cfg.ServiceGrpcHost+cfg.ServiceGrpcPort)
	if err != nil{
		fmt.Println("Error while listening grpc host port!",err.Error())
		return
	}

	fmt.Println("Service is running..., grpc port", cfg.ServiceGrpcPort)
	err = grpcServer.Serve(lis)
	if err != nil{
		fmt.Println("Error while listening grpc", err.Error())
 	}
}

