#!/bin/bash

# Get the directory of the script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Go up one directory to the parent of the scripts folder
PARENT_DIR=$(dirname "${DIR}")

# Ensure the genproto directory exists
mkdir -p "${PARENT_DIR}/genproto"

# List of directories containing .proto files
proto_directories=("apple_store_protos/storage_service_protos" "apple_store_protos/order_service_protos")

# Loop through each directory
for proto_dir in "${proto_directories[@]}"; do
  # Get the subfolder name (e.g., catalog_service_protos)
  subfolder=$(basename "${proto_dir}")

  # Create the corresponding directory structure in genproto
  mkdir -p "${PARENT_DIR}/genproto/${subfolder}"

  # Loop through all .proto files in the directory
  for file in "${proto_dir}"/*.proto; do
    if [ -f "$file" ]; then
      # Get the filename without the extension
      filename=$(basename -- "$file")
      filename_without_extension="${filename%.*}"

      # Run protoc for the current file
      protoc -I "${proto_dir}" "$file" --go_out=paths=source_relative:"${PARENT_DIR}/genproto/${subfolder}" --go-grpc_out=paths=source_relative:"${PARENT_DIR}/genproto/${subfolder}"
    fi
  done
done
