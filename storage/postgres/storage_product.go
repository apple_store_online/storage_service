package postgres

import (
	"context"
	"fmt"
	pb "storage_service/genproto/storage_service_protos"
	"storage_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type storageProductRepo struct {
	DB *pgxpool.Pool
}

func NewStorageProductRepo(db *pgxpool.Pool) storage.IStorageProduct {
	return &storageProductRepo{
		DB: db,
	}
}

func (s *storageProductRepo) Create(ctx context.Context, createStProduct *pb.CreateStorageProductRequest) (*pb.StorageProduct,error) {
	uid := uuid.New()
	updStProduct := pb.StorageProduct{}

	query := `INSERT into storage_products (id, product_id, quantity, color, original_price, selling_price)
		values ($1, $2, $3, $4, $5, $6)
	returning id, product_id, quantity, color, original_price, selling_price, created_at::text `

	err := s.DB.QueryRow(ctx, query, 
		uid,
		createStProduct.GetProductId(),
		createStProduct.GetQuantity(),
		createStProduct.GetColor(),
		createStProduct.GetOriginalPrice(),
		createStProduct.GetSellingPrice(),
	).Scan(
		&updStProduct.Id,
		&updStProduct.ProductId,
		&updStProduct.Quantity,
		&updStProduct.Color,
		&updStProduct.OriginalPrice,
		&updStProduct.SellingPrice,
		&updStProduct.CreatedAt,
	)

	if err != nil{
		fmt.Println("error while creating storage product!", err.Error())
		return &pb.StorageProduct{}, err
	}

	return &updStProduct, nil
}

func (s *storageProductRepo) Get(ctx context.Context, pKey *pb.StorageProductPrimaryKey) (*pb.StorageProduct, error) {
	updStProduct := pb.StorageProduct{}

	query := `SELECT id, product_id, quantity, color, original_price, selling_price, created_at::text, updated_at::text
			from storage_products WHERE id = $1 AND deleted_at = 0 `
	
	err := s.DB.QueryRow(ctx,query,pKey.GetId()).Scan(
		&updStProduct.Id,
		&updStProduct.ProductId,
		&updStProduct.Quantity,
		&updStProduct.Color,
		&updStProduct.OriginalPrice,
		&updStProduct.SellingPrice,
		&updStProduct.CreatedAt,
		&updStProduct.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting storage product!", err.Error())
		return &pb.StorageProduct{},err
	}

	return &updStProduct, nil
}

func (s *storageProductRepo) GetList(ctx context.Context, req *pb.GetStorageProductListRequest) (*pb.StorageProductsResponse, error) {
	
	var(
		search = req.GetSearch()
		count int32
		offset = (req.GetPage() - 1) * req.GetLimit()
		storageProducts = pb.StorageProductsResponse{}
	)

	countQuery := `SELECT count(1) from storage_products as sp 
	INNER JOIN products ON sp.product_id = products.id
		WHERE sp.deleted_at = 0 AND products.deleted_at = 0 `
	if search != ""{
		countQuery += fmt.Sprintf(`AND products.name ilike '%%%s%%' `,search)
	}
	
	err := s.DB.QueryRow(ctx, countQuery).Scan(
		&count,
	)
	if err != nil{
		fmt.Println("error while scanning count query!", err.Error())
		return &pb.StorageProductsResponse{}, err
	}

	query := `SELECT sp.id, products.name, sp.quantity, sp.color, sp.original_price, sp.selling_price from storage_products as sp 
		INNER JOIN products ON sp.product_id = products.id 
	WHERE sp.deleted_at = 0 AND products.deleted_at = 0 `

	if search != ""{
		query += fmt.Sprintf(`AND products.name ilike '%%%s%%' `, search)
	}

	query += ` LIMIT $1 OFFSET $2 `

	rows, err := s.DB.Query(ctx, query, req.GetLimit(), offset)
	if err != nil{
		fmt.Println("error while query rows!", err.Error())
		return &pb.StorageProductsResponse{}, err
	}
	for rows.Next(){
		storageProduct := pb.StorageProduct{}

		err := rows.Scan(&storageProduct)
		if err != nil{
			fmt.Println("error while scanning rows!", err.Error())
			return &pb.StorageProductsResponse{}, err
		}

		storageProducts.StorageProducts = append(storageProducts.StorageProducts, &storageProduct)
	}

	storageProducts.Count = count

	return &storageProducts, nil
}

func (s *storageProductRepo) Update(ctx context.Context, updStProduct *pb.StorageProduct) (*pb.StorageProduct, error) {
	stProduct := pb.StorageProduct{}

	query := `UPDATE storage_products SET product_id = $1, quantity = $2, color = $3
		original_price = $4, selling_price = $5, updated_at = now() 
	WHERE id = $6 AND deleted_at = 0 
			returning id, product_id, quantity, color, original_price, selling_price, updated_at::text `

	err := s.DB.QueryRow(ctx, query,
		updStProduct.GetProductId(),
		updStProduct.GetQuantity(),
		updStProduct.GetColor(),
		updStProduct.GetOriginalPrice(),
		updStProduct.GetSellingPrice(),
		updStProduct.GetId(),
	).Scan(
		&stProduct.Id,
		&stProduct.ProductId,
		&stProduct.Quantity,
		&stProduct.Color,
		&stProduct.OriginalPrice,
		&stProduct.SellingPrice,
		&stProduct.UpdatedAt,
	)
	
	if err != nil{
		fmt.Println("error while updating storage product!", err.Error())
		return &pb.StorageProduct{}, err
	}

	return &stProduct, nil
}

func (s *storageProductRepo) Delete(ctx context.Context, pKey *pb.StorageProductPrimaryKey) (error) {
	query := `UPDATE storage_products SET deleted_at = 0 
		WHERE id = $1 AND deleted_at = 0 `
	
	_, err := s.DB.Exec(ctx, query, pKey.GetId())
	if err != nil{
		fmt.Println("error while deleting storage product!", err.Error())
		return err
	}

	return nil
}


