package postgres

import (
	"context"
	"fmt"
	pb "storage_service/genproto/storage_service_protos"
	"storage_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type productRepo struct {
	DB *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) storage.IProductStorage {
	return &productRepo{
		DB: db,
	}
}

func (p *productRepo) Create(ctx context.Context, createProduct *pb.CreateProductRequest) (*pb.Product, error) {
	query := `INSERT INTO products (id, name, category_id)
			values ($1, $2, $3) 
		returning id, name, created_at::text `

	uid := uuid.New()
	product := pb.Product{}
	
	err := p.DB.QueryRow(ctx, query, 
		uid,
		createProduct.GetName(),
		createProduct.GetCategoryId(),
	).Scan(
		&product.Id,
		&product.Name,
		&product.CreatedAt,
	)

	if err != nil{
		fmt.Println("error while creating product!",err.Error())
		return &pb.Product{}, nil
	}

	return &product, nil
}

func (p *productRepo) Get(ctx context.Context, pKey *pb.ProductPrimaryKey) (*pb.Product, error) {
	query := `SELECT id, name, category_id, created_at::text, updated_at:text 
			from products where id = $1 AND deleted_at = 0 `
	
	product := pb.Product{}

	err := p.DB.QueryRow(ctx,query, pKey.GetId()).Scan(
		&product.Id,
		&product.Name,
		&product.CategoryId,
		&product.CreatedAt,
		&product.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while getting product!", err.Error())
		return &pb.Product{},err
	}

	return &product, nil
}

func (p *productRepo) GetList(ctx context.Context, req *pb.GetProductListRequest) (*pb.ProductsResponse, error)  {
	
	var(
		search = req.GetSearch()
		count int32
		offset = (req.GetPage() - 1) * req.GetLimit()
		products = pb.ProductsResponse{}
	)

	countQuery := `SELECT count(1) from products where deleted_at = 0 `

	if search != ""{
		countQuery += fmt.Sprintf(` AND name ilike '%%%s%%' `, search)
	}

	err := p.DB.QueryRow(ctx, countQuery).Scan(
		&count,
	)
	if err != nil{
		fmt.Println("error while getting count of products!",err.Error())
		return &pb.ProductsResponse{},err
	}

	query := `SELECT id, name, category_id, created_at::text, updated_at::text
			from products where deleted_at = 0 `
	
	if search != ""{
		query += fmt.Sprintf(`AND name ilike '%%%s%%' `, search)
	}

	query += `LIMIT $1 OFFSET $2 `

	rows, err := p.DB.Query(ctx, query, req.GetLimit(), offset)
	if err != nil{
		fmt.Println("error while query rows!", err.Error())
		return &pb.ProductsResponse{}, err
	}

	for rows.Next() {
		product := pb.Product{}

		err := rows.Scan(
			&product.Id,
			&product.Name,
			&product.CategoryId,
			&product.CreatedAt,
			&product.UpdatedAt,
		)
		if err != nil{
			fmt.Println("error while scanning rows!", err.Error())
			return &pb.ProductsResponse{},err
		}

		products.Products = append(products.Products, &product)
	}

	products.Count = count
	return &products, nil
}

func (p *productRepo) Update(ctx context.Context, updProduct *pb.Product) (*pb.Product, error) {
	product := pb.Product{}

	query := `UPDATE products SET name = $1, category_id = $2, updated_at = now() 
			WHERE id = $3 AND deleted_at = 0 
		returning id, name, category_id, updated_at::text `

	err := p.DB.QueryRow(ctx,query,
		updProduct.GetName(),
		updProduct.GetCategoryId(),
		updProduct.GetId(),
	).Scan(
		&product.Id,
		&product.Name,
		&product.CategoryId,
		&product.UpdatedAt,
	)
	if err != nil{
		fmt.Println("error while updating products!",err.Error())
		return &pb.Product{}, err
	}

	return &product, nil
}

func (p *productRepo) Delete(ctx context.Context, pKey *pb.ProductPrimaryKey) (error) {
	query := `UPDATE products SET deleted_at = 1 WHERE id = $1 `

	_, err := p.DB.Exec(ctx,query, pKey.GetId())
	if err != nil{
		fmt.Println("error while deleting product!", err.Error())
		return err
	}
	return nil
}