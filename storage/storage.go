package storage

import (
	"context"
	pb "storage_service/genproto/storage_service_protos"
)

type IStorage interface {
	Close()
	Category() ICategoryStorage
	Product() IProductStorage
	StorageProduct() IStorageProduct
}

type ICategoryStorage interface {
	Create(context.Context, *pb.CreateCategoryRequest) (*pb.Category, error)
	Get(context.Context, *pb.CategoryPrimaryKey) (*pb.Category, error)
	GetList(context.Context, *pb.GetCategoryListRequest) (*pb.CategoriesResponse, error)
	Update(context.Context, *pb.Category) (*pb.Category, error)
	Delete(context.Context, *pb.CategoryPrimaryKey) (error)
}

type IProductStorage interface {
	Create(context.Context, *pb.CreateProductRequest) (*pb.Product, error)
	Get(context.Context, *pb.ProductPrimaryKey) (*pb.Product, error)
	GetList(context.Context, *pb.GetProductListRequest) (*pb.ProductsResponse, error)
	Update(context.Context, *pb.Product) (*pb.Product, error)
	Delete(context.Context, *pb.ProductPrimaryKey) (error)
}

type IStorageProduct interface {
	Create(context.Context, *pb.CreateStorageProductRequest) (*pb.StorageProduct, error)
	Get(context.Context, *pb.StorageProductPrimaryKey) (*pb.StorageProduct, error)
	GetList(context.Context, *pb.GetStorageProductListRequest) (*pb.StorageProductsResponse, error)
	Update(context.Context, *pb.StorageProduct) (*pb.StorageProduct, error)
	Delete(context.Context, *pb.StorageProductPrimaryKey) (error)
}