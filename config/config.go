package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

type Config struct {
	ServiceName string
	Environment string

	PostgresHost string
	PostgresPort string
	PostgresUser string
	PostgresPassword string
	PostgresDB string

	ServiceGrpcHost string
	ServiceGrpcPort string
}

func Load() Config{
	cfg := Config{}

	err := godotenv.Load()
	if err != nil{
		fmt.Println("Error while loading from godotenv!", err.Error())
		return Config{}
	}

	cfg.ServiceName = cast.ToString(getOrReturnDefault("SERVICE_NAME", "storage"))
	cfg.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "dev"))

	cfg.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST","localhost"))
	cfg.PostgresPort = cast.ToString(getOrReturnDefault("POSTGRES_PORT", "5432"))
	cfg.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "your_user"))
	cfg.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "your_password"))
	cfg.PostgresDB = cast.ToString(getOrReturnDefault("POSTGRES_DB", "your_database_name"))

	cfg.ServiceGrpcHost = cast.ToString(getOrReturnDefault("SERVICE_GRPC_HOST", "localhost"))
	cfg.ServiceGrpcPort = cast.ToString(getOrReturnDefault("SERVICE_GRPC_PORT", "8080"))

	return cfg
}

func getOrReturnDefault (key string, defaultValue interface{})interface{}{
	value := os.Getenv(key)
	if value != ""{
		return value
	}
	return defaultValue
}
