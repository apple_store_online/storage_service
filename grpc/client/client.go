package client

import (
	"fmt"
	"storage_service/config"
	storage_service "storage_service/genproto/storage_service_protos"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	CategoryService() storage_service.CategoryServiceClient
	ProductService() storage_service.ProductServiceClient
	StorageProductService() storage_service.StorageProductServiceClient
}

type grpcClients struct {
	categoryService storage_service.CategoryServiceClient
	productService storage_service.ProductServiceClient
	storageProductService storage_service.StorageProductServiceClient
}

func NewGrpcClients (cfg config.Config)(IServiceManager, error){
	connStorageService, err := grpc.Dial(
		cfg.ServiceGrpcHost+cfg.ServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil{
		fmt.Println("Error while opening a dial for storage service!",err.Error())
		return nil, err
	}

	return &grpcClients{
		categoryService: storage_service.NewCategoryServiceClient(connStorageService),
		productService: storage_service.NewProductServiceClient(connStorageService),
		storageProductService: storage_service.NewStorageProductServiceClient(connStorageService),
	}, nil

}

func (g *grpcClients) CategoryService() storage_service.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) ProductService() storage_service.ProductServiceClient {
	return g.productService
}

func (g *grpcClients) StorageProductService() storage_service.StorageProductServiceClient {
	return g.storageProductService
}
