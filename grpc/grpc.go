package grpc

import (
	pb "storage_service/genproto/storage_service_protos"
	"storage_service/grpc/client"
	"storage_service/service"
	"storage_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
 )

func SetUpServer (strg storage.IStorage, services client.IServiceManager)*grpc.Server{
	grpcServer := grpc.NewServer()

	pb.RegisterCategoryServiceServer(grpcServer, service.NewCategoryService(strg, services))
	pb.RegisterProductServiceServer(grpcServer,service.NewProductService(strg, services))
	pb.RegisterStorageProductServiceServer(grpcServer, service.NewStorageProductService(strg,services))

	reflection.Register(grpcServer)

	return grpcServer
}